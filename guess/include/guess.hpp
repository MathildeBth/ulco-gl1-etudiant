#ifndef GUESS_HPP
#define GUESS_HPP

#include <iostream>
#include <cstdlib>
#include <string.h>
#include <stdio.h>

/*! \mainpage Documentation du jeu Guess Game
 *
 * Guess est un jeu ou il faut trouver le bon nombre entre 1 et 100. 
 * 
 * Le nombre du début sera toujours choisi aléatoirement.
 */

class Guess {
private:
    std::string historique;
    unsigned int random;
    unsigned int nbEssais;
    bool victoire;
public:
    Guess();
    bool devinerNombre(unsigned int nombre);
    void lancerPartie();
};

#endif