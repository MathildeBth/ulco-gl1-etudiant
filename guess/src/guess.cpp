#include "../include/guess.hpp"

#include <iostream>
#include <cstdlib>
#include <string.h>
#include <stdio.h>
#include <random>

Guess::Guess(){
    historique = "historique :";
    std::random_device r;
    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(1, 100);
    random = uniform_dist(e1);
    nbEssais = 5;
}

void Guess::lancerPartie() {

    while(!victoire && nbEssais > 0) {
        std::cout << historique << std::endl;
        std::cout << "Entrer un nombre entre 1 et 100 : ";
        std::cout.flush();
        std::string ligne;

        if (not std::getline(std::cin, ligne))
            throw std::invalid_argument("mauvais caractère");
        try {
            historique = historique + " " + ligne;
            victoire = devinerNombre(std::stoi(ligne));
            std::cout << std::endl;
        }
        catch (...) {
            std::cout << "erreur" << std::endl;
        }
    }

    if(victoire) 
        std::cout << "Vous avez gagné !" << std::endl;
    else
    {
        std::cout << "Vous avez perdu..." << std::endl;
        std::cout << "NombreSecret : "  << random << std::endl;
    } 
}

bool Guess::devinerNombre(unsigned int nombre) {
    if(random < 1 || random > 100)  {
        std::cout << "nombre invalide" << std::endl;
        return false;
    }

    if(nombre < random) {
        std::cout << "plus haut" << std::endl;
        nbEssais = nbEssais - 1;
        return false;
    }

    if(nombre > random) {
        std::cout << "plus bas"  << std::endl;
        nbEssais = nbEssais - 1;
        return false;
    }

    return true;
}
