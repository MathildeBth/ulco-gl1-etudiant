#include <catch2/catch.hpp>
#include <app/Guess.hpp>

TEST_CASE( "test 1" ) {
    REQUIRE( mul2(0) == 0 );
}

TEST_CASE( "test 2" ) {
    REQUIRE( mul2(21) == 42 );
}

