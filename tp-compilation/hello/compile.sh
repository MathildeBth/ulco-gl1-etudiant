CXXFLAGS = -Wall -Wextra -O2
LDFLAGS = 
OBJ = main.o hello.o

say-hello: $(OBJ)
    c++ -o $@ $(CXXFLAGS) $^(LDFLAGS)

clean:
    rm -f $(OBJ) say-hello