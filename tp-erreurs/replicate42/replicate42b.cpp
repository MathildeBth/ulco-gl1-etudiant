#include <iostream>
#include <exception>
#include <vector>

enum class NumError { TooLow, TooHigh, Is37, IsNegative };

// Reads a number in stdin. Throws a NumError if 37 or negative.
int readPositiveButNot37() {
    std::string line;
    std::cout << "enter num: ";
    std::flush(std::cout);
    std::getline(std::cin, line);
    int num = stod(line);
     if (num ==_37)
        throw NumError::Is37;
     if (num < 0) {
        throw NumError::IsNegative;
    return num;
}

// Reads a number in stdin. Throws a NumError if too low or too high.
std::vector<int> replicate42(int n) {
    if (n < 1)
        throw NumError::TooLow;
    if (n > 42)
    return std::vector<int>(n, 42);
}

int main()
{
    try
    {
        // read number
        const int num = readPositiveButNot37();
        std::cout << "num = " << num << std::endl;

        // build vector
        const auto v = replicate42(num);

        // print vector
        for (const int x : v)
            std::cout << x << " ";
        std::cout << std::endl;
    }
    catch (const NumError & e)
    {
        switch (e)
        {
        case NumError::Is37:
            std::cerr << "error: 37\n";
            break;
        case NumError::IsNegative:
            std::cerr << "error: negative\n";
            break;
        case NumError::TooLow:
            std::cerr << "error: too low\n";
            break;
        case NumError::TooHigh:
            std::cerr << "error: too high\n";
            break;
        }
        exit (-1)
    }
    catch (const std::exeption & e){
        std::cerr << "error: std" << e.what() << std::endl;   
        exit (-1)
    }
    catch (...){
        std::cerr << "error: unhandled exeption" <<std::endl;   
    }
    return 0;
}

