with import <nixpkgs> {};
clangStdenv.mkDerivation {
    name = "selectfile";
    src = ./.;
    buildInputs = [
        cmake
        pkgconfig
        glog
        gtkmm3
    ];
}
