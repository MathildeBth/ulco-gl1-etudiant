#include <catch2/catch.hpp>

#include <mymaths/add.hpp>

TEST_CASE ( "test nul2" ){
    REQUIRE( mul2(21) == 42);
}

TEST_CASE( "test muln" ){
    REQUIRE( muln(21, 2) == 42);
}