#include "Bottle.hpp"

#include <sstream>

double alcoholVolume(const Bottle & b) {
    return (b._deg * (b._vol*100))/100;
}

double alcoholVolume(const std::vector<Bottle> & bs) {
    double volume = 0;

    for(int i = 0; i<bs.size(); i++){
        volume = bs[i]._vol + 1;
    }

    return volume;
}

bool isPoivrot(const std::vector<Bottle> & bs) {
    return alcoholVolume(bs) > 0.1;
}

std::vector<Bottle> readBottles(std::istream & is) {
    // TODO
    return {};
}

